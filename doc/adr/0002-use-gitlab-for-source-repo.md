# 2. Use Gitlab for Source Repo

Date: 2023-02-13

## Status

Accepted

## Context

Gitlab is not available for on-premise.
Gitlab can be used in on-premise hosting.
Gitlab is open source.

## Decision

We decide Source Repo Gitlab cloud.

## Consequences

We will use git cloud until MVP version.
Infra team will setup on-premise Gitlab server.
