# 3. VSCode for IDE

Date: 2023-02-13

## Status

Superseded by [4. intallij IDE](0004-intallij-ide.md)

## Context

IDEs
  - vscode : free
  - intellij : commercial
  - eclipse : free

## Decision

We will use VSCode IDE

## Consequences

QA team should make coding convention to vs setting file.
